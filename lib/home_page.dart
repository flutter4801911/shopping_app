import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop_app/items_bottemsheet.dart';
import 'package:shop_app/product_page.dart';
import 'package:shop_app/sigin_page.dart';
import 'package:shop_app/utility.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Container(
        padding: const EdgeInsets.only(top: 10, left: 13, right: 13),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 7),
              child: Row(
                children: [
                  const Icon(Icons.menu_outlined),
                  const Spacer(),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(60),
                    child: Image.network(
                      "https://media.tenor.com/ubYVdzOBtPQAAAAM/hitman-show-rohit-sharma.gif",
                      height: 35,
                      width: 35,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const SizedBox(
                    width: 25,
                  ),
                  GestureDetector(
                    child: const Icon(Icons.logout_rounded),
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return const LoginAndSign();
                      }));
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          backgroundColor: Colors.green,
                          content: Text(
                            "Logut successfully",
                            style: GoogleFonts.quicksand(color: Colors.white),
                          )));
                    },
                  ),
                  const SizedBox(
                    width: 25,
                  ),
                  const Icon(Icons.shopping_basket_outlined),
                ],
              ),
            ),
            Expanded(
                child: Container(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30)),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        blurRadius: 12,
                        spreadRadius: 10)
                  ]),
              child: ListView.separated(
                separatorBuilder: (context, index) => const SizedBox(
                  height: 15,
                ),
                itemCount: productList.length,
                itemBuilder: (BuildContext context, int index) {
                  int x = index;
                  int y = x + 1;

                  return (index % 2 == 0)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            (x < productList.length)
                                ? GestureDetector(
                                    onTap: () {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                        builder: (context) => ProductInfo(
                                            seleProduct: productList[x]),
                                      ));
                                    },
                                    child: Container(
                                      height: 260,
                                      width: 175,
                                      decoration: BoxDecoration(
                                          color: const Color.fromARGB(
                                                  255, 236, 200, 245)
                                              .withOpacity(0.8),
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      padding: const EdgeInsets.only(
                                          top: 25,
                                          left: 12,
                                          right: 12,
                                          bottom: 12),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(30),
                                            child: Image.network(
                                              productList[x].imgUrl,
                                              fit: BoxFit.cover,
                                              height: 150,
                                              width: double.infinity,
                                            ),
                                          ),
                                          Text(productList[x].productName),
                                          Row(
                                            children: [
                                              Text(
                                                "₹ ${productList[x].price}",
                                              ),
                                              const Spacer(),
                                              const Icon(
                                                Icons.star_purple500_sharp,
                                                color: Color.fromRGBO(
                                                    255, 215, 0, 1),
                                              ),
                                              Text("${productList[x].rating}")
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              GestureDetector(
                                                  onTap: () {
                                                    if (productList[x]
                                                            .quantity >
                                                        0) {
                                                      productList[x].quantity--;
                                                      if (productList[x]
                                                              .quantity ==
                                                          0) {
                                                        buyList.remove(
                                                            productList[x]);
                                                      }
                                                      setState(() {});
                                                    }
                                                  },
                                                  child: Container(
                                                      decoration:
                                                          const BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              color:
                                                                  Colors.white),
                                                      child: const Icon(Icons
                                                          .exposure_minus_1_rounded))),
                                              const Spacer(),
                                              Container(
                                                  height: 20,
                                                  width: 60,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              30)),
                                                  child: Center(
                                                      child: Text(
                                                          "${productList[x].quantity}"))),
                                              const Spacer(),
                                              GestureDetector(
                                                  onTap: () {
                                                    productList[x].quantity++;
                                                    buyList.add(productList[x]);
                                                    setState(() {});
                                                  },
                                                  child: Container(
                                                      decoration:
                                                          const BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              color:
                                                                  Colors.white),
                                                      child: const Icon(Icons
                                                          .plus_one_rounded)))
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                : const SizedBox(),
                            (y < productList.length)
                                ? GestureDetector(
                                  onTap: (){
                                    Navigator.of(context)
                                          .push(MaterialPageRoute(
                                        builder: (context) => ProductInfo(
                                            seleProduct: productList[y]),
                                      ));
                                  },
                                  child: Container(
                                      height: 260,
                                      width: 175,
                                      decoration: BoxDecoration(
                                          color: const Color.fromARGB(
                                                  255, 236, 200, 245)
                                              .withOpacity(0.8),
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      padding: const EdgeInsets.all(12),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(30),
                                            child: Image.network(
                                              productList[y].imgUrl,
                                              fit: BoxFit.cover,
                                              height: 150,
                                              width: double.infinity,
                                            ),
                                          ),
                                          Text(productList[y].productName),
                                          Row(
                                            children: [
                                              Text(
                                                "₹ ${productList[y].price}",
                                              ),
                                              const Spacer(),
                                              const Icon(
                                                Icons.star_purple500_sharp,
                                                color: Color.fromRGBO(
                                                    255, 215, 0, 1),
                                              ),
                                              Text("${productList[y].rating}")
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              GestureDetector(
                                                  onTap: () {
                                                    if (productList[y].quantity >
                                                        0) {
                                                      productList[y].quantity--;
                                                      if (productList[y]
                                                              .quantity ==
                                                          0) {
                                                        buyList.remove(
                                                            productList[y]);
                                                      }
                                                      setState(() {});
                                                    }
                                                  },
                                                  child: Container(
                                                      decoration:
                                                          const BoxDecoration(
                                                              shape:
                                                                  BoxShape.circle,
                                                              color:
                                                                  Colors.white),
                                                      child: const Icon(Icons
                                                          .exposure_minus_1_rounded))),
                                              const Spacer(),
                                              Container(
                                                  height: 20,
                                                  width: 60,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              30)),
                                                  child: Center(
                                                      child: Text(
                                                          "${productList[y].quantity}"))),
                                              const Spacer(),
                                              GestureDetector(
                                                  onTap: () {
                                                    productList[y].quantity++;
                                                    buyList.add(productList[y]);
                                                    setState(() {});
                                                  },
                                                  child: Container(
                                                      decoration:
                                                          const BoxDecoration(
                                                              shape:
                                                                  BoxShape.circle,
                                                              color:
                                                                  Colors.white),
                                                      child: const Icon(Icons
                                                          .plus_one_rounded)))
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                )
                                : const SizedBox()
                          ],
                        )
                      : const SizedBox();
                },
              ),
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            print(buyList);
            DisplayBottomSheetState(
                    obj: context, selectedItemList: buyList.toList())
                .showSelectedItems();
            //buyList.clear();
          });
        },
        child: const Icon(Icons.shopping_cart_outlined),
      ),
    );
  }
}
