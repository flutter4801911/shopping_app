import 'package:flutter/material.dart';
import 'package:shop_app/utility.dart';

class DisplayBottomSheetState extends State {
  double? _tax;
  double? _subTotal;
  double? _deliveryCharge;
  BuildContext obj;
  List<Product> selectedItemList;
  DisplayBottomSheetState({required this.obj, required this.selectedItemList});
  double getTotalCost() {
    if (selectedItemList.isEmpty) {
      _tax = _deliveryCharge = _subTotal = 0;
      return 0.0;
    }
    double totalCost = 0;
    for (dynamic product in selectedItemList) {
      totalCost += product.price * product.quantity;
    }
    _tax = totalCost * 0.03;
    _subTotal = totalCost;
    _deliveryCharge = 100.0;
    return totalCost + _deliveryCharge! + _tax!;
  }

  void showSelectedItems() {
    showModalBottomSheet(
        context: obj,
        builder: (BuildContext context) {
          return Column(
            children: [
              Row(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(obj).pop();
                      },
                      icon: const Icon(Icons.arrow_downward_outlined)),
                  const Text(
                    "BASKET",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: Color.fromRGBO(144, 108, 141, 1)),
                  ),
                  const Spacer(),
                  const Text(" 9Items "),
                ],
              ),
              Expanded(
                  child: ListView.separated(
                      itemBuilder: (context, index) {
                        return (index == selectedItemList.length)
                            ? Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        const Text("Total"),
                                        const Spacer(),
                                        Text(
                                          "${getTotalCost()}",
                                          style: const TextStyle(
                                            fontSize: 40,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        const Text("Subtotal :"),
                                        const Spacer(),
                                        Text(
                                          "$_subTotal",
                                          style: const TextStyle(
                                            fontSize: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        const Text("Delivery :"),
                                        const Spacer(),
                                        Text(
                                          "$_deliveryCharge",
                                          style: const TextStyle(
                                            fontSize: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        const Text("Tax :"),
                                        const Spacer(),
                                        Text(
                                          "$_tax",
                                          style: const TextStyle(
                                            fontSize: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                            )
                            : Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    height: 100,
                                    width: 100,
                                    child: Image.network(
                                        selectedItemList[index].imgUrl),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          "Quantity : ${selectedItemList[index].quantity}"),
                                      Text(selectedItemList[index].productName),
                                    ],
                                  ),
                                  const Spacer(),
                                  Text("${selectedItemList[index].price}"),
                                ],
                              );
                      },
                      separatorBuilder: (context, index) {
                        return const Divider();
                      },
                      itemCount: selectedItemList.length + 1)),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, right: 30, bottom: 10, top: 10),
                child: ElevatedButton(
                  onPressed: () {
                    selectedItemList.clear();
                    buyList.clear();
                    for (dynamic data in productList) {
                      data.quantity = 0;
                    }
                    Navigator.of(obj).pop();
                    FocusScope.of(obj).unfocus();
                  },
                  style: const ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Color.fromRGBO(79, 59, 93, 1)),
                    minimumSize:
                        MaterialStatePropertyAll(Size(double.infinity, 50)),
                    shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)))),
                  ),
                  child: const Text(
                    "Clear Basket",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
            onPressed: () {
              showSelectedItems();
            },
            child: const Text("Items")),
      ),
    );
  }
}
