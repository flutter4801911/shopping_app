List<Product> productList = [
  Product(
      productName: "Mango",
      price: 80,
      rating: 4.5,
      imgUrl:
          "https://5.imimg.com/data5/SELLER/Default/2023/9/344928632/OW/RQ/XC/25352890/yellow-mango.jpeg"),
  Product(
      productName: "apple",
      price: 40,
      rating: 4.1,
      imgUrl:
          "https://thumbs.dreamstime.com/b/red-apple-leaf-slice-white-background-29914331.jpg"),
  Product(
      productName: "banana",
      price: 15,
      rating: 3.6,
      imgUrl:
          "https://th-thumbnailer.cdn-si-edu.com/4Nq8HbTKgX6djk07DqHqRsRuFq0=/1000x750/filters:no_upscale()/https://tf-cmsv2-smithsonianmag-media.s3.amazonaws.com/filer/d5/24/d5243019-e0fc-4b3c-8cdb-48e22f38bff2/istock-183380744.jpg"),
  Product(
      productName: "Orange",
      price: 60,
      rating: 3.9,
      imgUrl:
          "https://t3.gstatic.com/licensed-image?q=tbn:ANd9GcS_l2nKIkDzVLzxONc371GJWkhKWsk-uqTVFau7nskckDvQlqk4Ka2KytgbdCPDld-J"),
  Product(
      productName: "lemon",
      price: 10,
      rating: 3.9,
      imgUrl:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Lemon.jpg/1024px-Lemon.jpg"),
  
];

class Product {
  String productName;
  int price;
  double rating;
  int quantity = 0;
  String imgUrl;
  Product(
      {required this.productName,
      required this.price,
      required this.rating,
      required this.imgUrl});

  @override
  String toString() {
    return "name: $productName  ,quantity: $quantity";
  }
}

Set<Product> buyList = {};
