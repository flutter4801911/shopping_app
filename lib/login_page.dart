import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget{
    const LoginPage({super.key});

    @override
    State<LoginPage> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage>{

    // BuildContext loginContext;

    // LoginPageState({required this.loginContext});
  final TextEditingController _userNameControll = TextEditingController();
  final TextEditingController _passControll = TextEditingController();
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  final FocusNode _passFocus = FocusNode();
  bool _hiddenPass = true;

     void showLoginBottomSheet() {
    showModalBottomSheet(
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        scrollControlDisabledMaxHeightRatio: 0.45,
        backgroundColor: const Color.fromARGB(35, 254, 254, 254),
        context: context,
        builder: (BuildContext context) {
          return Form(
            key: _globalKey,
            child: Container(
              padding: MediaQuery.viewInsetsOf(context),
              decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.transparent,
                      blurRadius: 10,
                    )
                  ],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30)),
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Color.fromARGB(99, 255, 255, 255),
                        Color.fromARGB(169, 0, 0, 0),
                      ])),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 20, left: 12, right: 12, bottom: 10),
                    child: TextFormField(
                      controller: _userNameControll,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      showCursor: true,
                      decoration: const InputDecoration(
                          counterText: "Forgot Username ?",
                          counterStyle:
                              TextStyle(color: Colors.white, fontSize: 13),
                          enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(color: Colors.white30)),
                          hintText: "enter username or email ",
                          labelText: "Username or Email",
                          prefixIcon: Icon(Icons.supervised_user_circle),
                          prefixIconColor: Color.fromRGBO(234, 190, 226, 1),
                          labelStyle: TextStyle(color: Colors.white),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                  width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)))),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter valid username";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10, left: 12, right: 12, bottom: 10),
                    child: TextFormField(
                      controller: _passControll,
                      focusNode: _passFocus,
                      keyboardType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.done,
                      showCursor: true,
                      obscureText: _hiddenPass,
                      obscuringCharacter: "*",
                      decoration: InputDecoration(
                          counterText: "Forgot Password ?",
                          counterStyle: const TextStyle(
                              color: Colors.white, fontSize: 13),
                          enabledBorder: const OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(color: Colors.white30)),
                          hintText: "enter your passward",
                          labelText: "Passward",
                          prefixIcon: const Icon(Icons.lock_outlined),
                          suffixIcon: IconButton(
                              onPressed: () {
                                
                              },
                              icon: Icon((_hiddenPass)
                                  ? Icons.remove_red_eye_outlined
                                  : Icons.cancel_outlined)),
                          suffixIconColor:
                              const Color.fromRGBO(234, 190, 226, 1),
                          labelStyle: const TextStyle(color: Colors.white),
                          prefixIconColor:
                              const Color.fromRGBO(234, 190, 226, 1),
                          border: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Color.fromRGBO(0, 139, 148, 1),
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                  width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)))),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter valid password";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 30, right: 30),
                    child: ElevatedButton(
                      onPressed: () {

                      },
                      style: const ButtonStyle(
                        backgroundColor: MaterialStatePropertyAll(
                            Color.fromRGBO(0, 139, 148, 1)),
                        minimumSize:
                            MaterialStatePropertyAll(Size(double.infinity, 50)),
                        shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15)))),
                      ),
                      child: const Text(
                        "Login",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 60),
                    child: Divider(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.only(bottom: 15, right: 5),
                        decoration: BoxDecoration(
                            color: const Color.fromARGB(255, 245, 246, 247),
                            borderRadius: BorderRadius.circular(15)),
                        child: Image.network(
                            "https://img.icons8.com/?size=96&id=V5cGWnc9R4xj&format=png"),
                      ),
                      Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.only(left: 5, bottom: 15),
                        decoration: BoxDecoration(
                            color: const Color.fromARGB(255, 242, 243, 244),
                            borderRadius: BorderRadius.circular(15)),
                        child: Image.network(
                            "https://img.icons8.com/?size=100&id=30840&format=png"),
                      ),
                    ],
                  ),
                  const Text(
                    "Don't have an Account ?",
                    style: TextStyle(
                        color: Color.fromARGB(93, 255, 255, 255), fontSize: 17),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: GestureDetector(
                      child: const Text(
                        "SignUp",
                        style: TextStyle(
                            color: Color.fromRGBO(234, 190, 226, 1),
                            fontSize: 18),
                      ),
                      onTap: () {

                      },
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
    @override
    Widget build(BuildContext context){
        return Scaffold(
          body:ElevatedButton(onPressed: (){
            showLoginBottomSheet();
          }, child: const Text("Hiiii"))
        );
    }
}