import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shop_app/home_page.dart';
import 'package:shop_app/utility.dart';

class ProductInfo extends StatefulWidget {
  Product seleProduct;
  ProductInfo({super.key, required this.seleProduct});

  @override
  State<StatefulWidget> createState() {
    return _ProductInfoState(seleProduct: seleProduct);
  }
}

class _ProductInfoState extends State {
  Product seleProduct;
  _ProductInfoState({required this.seleProduct});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Container(
        padding: const EdgeInsets.only(top: 10, left: 13, right: 13),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 7),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const HomePage(),
                      ));
                    },
                    child: const Icon(Icons.arrow_back),
                  ),
                  const Spacer(),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(60),
                    child: Image.network(
                      "https://media.tenor.com/ubYVdzOBtPQAAAAM/hitman-show-rohit-sharma.gif",
                      height: 35,
                      width: 35,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const SizedBox(
                    width: 25,
                  ),
                  const SizedBox(
                    width: 25,
                  ),
                  const Icon(Icons.shopping_basket_outlined),
                ],
              ),
            ),
            Expanded(
                child: Container(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30)),
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        blurRadius: 12,
                        spreadRadius: 10)
                  ]),
            )),
          ],
        ),
      ),
    );
  }
}
